#!/usr/bin/env python

import fugashi
import os
import re
import sys


_RANGES = [
  {"from": ord(u"\u3300"), "to": ord(u"\u33ff")},         # compatibility ideographs
  {"from": ord(u"\ufe30"), "to": ord(u"\ufe4f")},         # compatibility ideographs
  {"from": ord(u"\uf900"), "to": ord(u"\ufaff")},         # compatibility ideographs
  {"from": ord(u"\U0002F800"), "to": ord(u"\U0002fa1f")}, # compatibility ideographs
  {"from": ord(u"\u3040"), "to": ord(u"\u309f")},         # Japanese Hiragana
  {"from": ord(u"\u30a0"), "to": ord(u"\u30ff")},         # Japanese Katakana
  {"from": ord(u"\u2e80"), "to": ord(u"\u2eff")},         # cjk radicals supplement
  {"from": ord(u"\u4e00"), "to": ord(u"\u9fff")},
  {"from": ord(u"\u3400"), "to": ord(u"\u4dbf")},
  {"from": ord(u"\U00020000"), "to": ord(u"\U0002a6df")},
  {"from": ord(u"\U0002a700"), "to": ord(u"\U0002b73f")},
  {"from": ord(u"\U0002b740"), "to": ord(u"\U0002b81f")},
  {"from": ord(u"\U0002b820"), "to": ord(u"\U0002ceaf")}  # included as of Unicode 8.0
]
RANGES = {
    "kana": (ord(u"\u3040"), ord(u"\u30ff")),
}


# The Tagger object holds state about the dictionary.
tagger = fugashi.Tagger()
jpn_re = re.compile(".*\b\d+\b\.(ja|jp|jpn)\..+")


def is_kana(string) -> bool:
    return all(RANGES["kana"][0] <= ord(x) <= RANGES["kana"][1] for x in string)


# consider using pysrt
def strip_srt(fd):
    lines = []
    line = True
    section = []
    while line:
        line = fd.readline()
        if not line:
            break
        elif line and line[0] == '\n':
            if section:
                lines.append("".join(section))
            section = []
            continue
        else:
            if line[0].isnumeric():
                continue
            section.append(line.strip())
    return lines


def subs_words(file_path, context=False) -> dict:
    with open(file_path, encoding="utf-8-sig") as fd:
        if file_path[-4:] == ".srt":
            lines = strip_srt(fd)

    # TODO get line word came from as context
    words = tagger("\n".join(lines))
    # print(dir(words[0].feature))
    # print(words[0].surface, words[0].feature.kana, words[0].feature.lemma)
    print("number of words:", len(words))
    words = {x.surface: x for x in words if x.surface.isalpha()}
    print("unique words:", len(words))
    # print([",".join([v.surface, v.feature.pron or ""]) for v in words.values()])
    return words


def dict_diff(a, b):
    """keys in a not in b"""
    a_keys = set(a.keys())
    b_keys = set(b.keys())
    diff_keys = a_keys - b_keys
    return {k: v for k, v in a.items() if k in diff_keys}


def vocab_lists(files, cumulative=True, context=False):
    # files.sort()
    if not files:
        raise FileNotFoundError
    file_words = {}
    for file_path in files:
        print(f"extracting words from: {file_path}")
        words = subs_words(file_path, context)
        if cumulative:
            for path, fwords in file_words.items():
                words = dict_diff(words, fwords)
            print("Cumulative new words: ", len(words))
        # print(words)
        # needed to avoid mismatch for some reason
        _ = [",".join([v.surface, v.feature.pron or ""]) for v in words.values()]
        file_words[file_path] = words
        print("======================================================")
    print("Total words: ", sum(len(x) for x in file_words.values()))
    # # file_words = {k: list(v).sort() for k, v in file_words}
    for path, fwords in file_words.items():
        with open(path + "_words.txt", 'w') as fd:
            fwords = [",".join([v.surface, v.feature.pron or "", v.feature.lemma or ""]) for v in fwords.values()]
            fwords.sort()
            fd.write('\n'.join(fwords) + '\n')
        # print(fwords)
        # return


if __name__ == "__main__":
    vocab_lists(sys.argv[1:])
