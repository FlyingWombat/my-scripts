#!/bin/sh

## makepkg-rec
# A directory recursive makepkg wrapper.
# It traverses the directory tree pre-order (depth first) and runs makepkg in any
# directory containing a PKGBUILD.
# This allows for easily building AUR packages with AUR dependency trees, by simply
# nesting PKGBUILD directories for each dependency in the directory tree.

set -e

_makepkg_rec(){
startdir="$1"
shift


pushd "$startdir" > /dev/null
for d in $(ls -d */ 2>/dev/null); do
    _makepkg_rec "$d" $@
done
if [ -f PKGBUILD ]; then
    echo =================================================================
    pwd
    echo $ makepkg $@
    makepkg $@
fi
popd > /dev/null
}

_makepkg_rec $@

# # a failed attempt at using find to scan the directory tree to avoid having to do
# # actual shell recursion.
# for pkgbuild in $(find "$startdir" -depth -type f -name PKGBUILD ); do
#     echo =================================================================
#     pushd "$(dirname $pkgbuild)"
#     echo $ makepkg --syncdeps --install --noconfirm $@
#     makepkg --syncdeps --install --noconfirm $@
#     popd
# done
