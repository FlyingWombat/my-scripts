#!/bin/sh
# updates sources for build repos
# Argument 1: source_path
#	directory in which to check for repos
#	if empty, then is current working directory
# Argument 2: --clean
#	flag to clean repos before updating (does not apply to bare repos)

if [ "$1" = "" ]; then
	source_path=$(pwd)
else
	source_path=$(cd "$1" && pwd)
fi

if [ "$2" = "--clean" ]; then
	clean="true"
else
	clean="false"
fi


# trap and die function:
#	this will abort the entire process tree if an error is encountered
#	or if `die` is called
trap "exit 1" TERM
export TOP_PID=$$

die()
{
   echo "Exit: Failure"
   kill -s TERM $TOP_PID
}

update(){
# update: updates git repo using remote update and merge, instead of pull
#		  this should be more stable for always making sure

	cd $1 || exit 1  # no need to die

	echo "remote update"
	git remote update --prune || die 			# update all remote tracking branches, and remove old

	# these don't work in bare repos:
	if [ "$(git rev-parse --is-bare-repository 2> /dev/null)" = "false" ]; then
		if [ "$clean" = "true" ]; then
			git clean -dxf
			git reset --hard HEAD
		fi
		echo "checkout master"
		git checkout master || die	 				# switch to master branch
		echo "merge"
		git merge --ff-only origin/master || die 	# do fast-forward merge of remote/master into current branch
		#git merge --strategy-option=theirs origin/master master || `git merge --abort; die`
	else
		# update master
		git fetch origin "master:master"
	fi

	cd ../
}

check(){
# check: recursively checks argument $1 and subdirectories
# 		 to see if they are git repos.
#	 	 If they are, updates the repo

	git_dir=$(git -C "$1" rev-parse --git-dir 2> /dev/null)

	if [ "$git_dir" = "." ] || [ "$git_dir" = ".git" ]; then
		echo "updating: $1"
		update "$1"
		echo "---------------------------------------------"
	else
		for repo in $1*/; do
			if [ "$repo" != "" ]; then
				check "$repo"
			fi
		done
	fi
}

check "$source_path"
