#!/bin/sh
# wrapper to apply my default settings to avifenc

set -e

call_args="$@"

abit=0  # audio bitrate; 0 activates default
acodec=copy  # audio codec
codec=libaom-av1
pix_fmt=yuv420p10le
crf=25
qp=
threads=0
tiles=0
speed=3
grain=0  # film-grain synthesis; higher setting has more aggressive denoising, and can erase fine detail.
fulldenoise=1  # switches denoise algorithm; light vs full
passes=1
pipe=
mem_threshold=85  # wait until used memory is less than this percent

for arg in $@; do
    case $arg in
        "-ca")
            acodec="$2"
            shift 2
            ;;
        "-ab")
            abit="$2"
            shift 2
            ;;
        "-c")
            codec="$2"
            shift 2
            ;;
        "-dn")
            grain="$2"
            shift 2
            ;;
        "-ldn")
            fulldenoise=0
            shift
            ;;
        "-f")
            overwrite=true
            shift
            ;;
        "-crf")
            crf="$2"
            shift 2
            ;;
        "-qp")
            qp="$2"
            shift 2
            ;;
        "-s")
            speed="$2"
            shift 2
            ;;
        "-j")
            threads="$2"
            shift 2
            ;;
        "-t")
            tiles="$2"
            shift 2
            ;;
        "-2pass")
            passes=2
            shift
            ;;
        "-pipe")
            # pipe to stand-alone encoder instead of ffmpeg builtin
            pipe=true
            shift
            ;;
        "-pix_fmt")
            pix_fmt=$2
            shift 2
            ;;
    esac
done
in="$1"
if [ -z "$2" ]; then
    out="${1%.*}.av1.mkv"
else
    out="$2"
fi
echo out $out

if [ -z "$overwrite" ] && [ -f "$out" ]; then
    echo "not overwriting output $out"
    exit
fi

# set nice priority of this process, and subsequently the encoders
renice -n 19 -p $$
ionice -c3 -p $$


# argument accumulator
ARGS=
if [ "$codec" = "libaom-av1" ]; then
    ARGS="$ARGS -c:v libaom-av1 -cpu-used $speed -threads $threads"
    ARGS="$ARGS -arnr-max-frames 3 -arnr-strength 1 -aq-mode 1 -denoise-noise-level $grain "
    AOMARGS="sb-size=64:enable-qm=1:deltaq-mode=0"
    if [ "$fulldenoise" = 0 ]; then
        AOMARGS="$AOMARGS:enable-dnl-denoising=0"
    fi
    ARGS="$ARGS -aom-params $AOMARGS -crf $crf"
elif [ "$codec" = "libsvtav1" ]; then
    ARGS="$ARGS -c:v libsvtav1 -preset $speed "
    SVTARGS="lp=$threads:tune=0:film-grain=$grain"
    if [ "$fulldenoise" = 0 ]; then
        SVTARGS="$SVTARGS:film-grain-denoise=0"
    fi
    ARGS="$ARGS -svtav1-params $SVTARGS -crf $crf"
elif [ "$codec" = "librav1e" ]; then
    if [ -z "$qp" ]; then
        qp=$(expr $crf \* 4)
    fi
    if [ "$threads" = 0 ]; then
        threads=2
    fi
    if [ "${tiles}" != 0 ]; then
        threads=$(expr $tiles + 1)
    fi
    ARGS="$ARGS -c:v librav1e -speed $speed -qp $qp -tiles $tiles"
    RAV1EARGS="threads=$threads:photon_noise=$grain"
    # if [ "$fulldenoise" = 0 ]; then
    #     RAV1EARGS="$RAV1EARGS:"
    # fi
    ARGS="$ARGS -rav1e-params $RAV1EARGS"
    if [ "$pipe" = "true" ]; then
        pass_args=
        if [ "$passes" = 2 ]; then
            pass_args="--second-pass /tmp/2pass_${in}.stats"
            ffmpeg -i "${in}" -map 0:v:0 -pix_fmt $pix_fmt -f yuv4mpegpipe -strict -1  - \
                | rav1e - --speed $speed --quantizer $qp --tiles $tiles --threads $threads --first-pass "/tmp/2pass_${in}.stats" -o /dev/null
        fi
        ffmpeg -i "${in}" -map 0:v:0 -pix_fmt $pix_fmt -f yuv4mpegpipe -strict -1  - \
            | rav1e - --speed $speed --quantizer $qp --photon-noise $grain --tiles $tiles --threads $threads $pass_args -o - \
            | ffmpeg -i - -i "${in}" -map 0:v -map 1 -map -1:v -c:v copy -c:a $acodec -b:a $abit  "$out"
        exit  ###############################################
    fi
fi

ARGS="$ARGS -g 240 -keyint_min 12 -pix_fmt yuv420p10le -b:v 0 -c:a $acodec -b:a $abit "
echo ARGS "$ARGS"

if [ "$passes" = 2 ]; then
	echo ffmpeg -i "$in" $ARGS -pass 1 -passlogfile "${in%.*}" -an -f null /dev/null
	ffmpeg -i "$in" $ARGS -pass 1 -passlogfile "${in%.*}" -an -f null /dev/null
	echo ffmpeg -i "$in" $ARGS -pass 2 -passlogfile "${in%.*}" "$out"
	ffmpeg -i "$in" $ARGS -pass 2 -passlogfile "${in%.*}" "$out"
else
	echo ffmpeg -i "$in" $ARGS "$out"
	ffmpeg -i "$in" $ARGS "$out"
fi
