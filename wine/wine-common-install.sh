#!/usr/bin/env bash

# this will use WINEPREFIX env var
if [ "$WINEPREFIX" == "" ]; then
    prefix_dir="$HOME/.wine" # defualt wine prefix
else
    prefix_dir=$WINEPREFIX
fi

# default values:
common_dir="$HOME/wine-common"
TEMPDIR="/tmp/wine-common-install-prefix"
CLEAN_SLATE=false
ADD_SHORTCUTS=false

## define args
# parse arguments:
PARAMS=""
while (( "$#" )); do
  case "$1" in
    -c|--clean)
    CLEAN_SLATE=true
    shift 1
    ;;
    -s|--shortcuts)
    ADD_SHORTCUTS=true
    shift 1
    ;;
    --common-dir)
      common_dir="$2"
      shift 2
      ;;
    --) # end argument parsing
#       echo end
      shift
      break
      ;;
    *) # preserve positional arguments
#       echo pos
      PARAMS="$PARAMS $1"
      shift
      ;;
  esac
done
# set positional arguments in their proper place
eval set -- "$PARAMS"

## validate args:
if [ ! -d $common_dir ]; then 
    if [[ "$common_dir" = "/home/"* || "$common_dir" = "/tmp/"* ]]; then
        echo "common directory does not exist: $common_dir"
        echo "making new common directory."
        wine-common-clear "$common_dir"
    else
        echo "invalid common dir path: $common_dir"
        exit 1
    fi
fi

# # check args:
# echo "Common dir: $common_dir"
# echo "Prefix dir: $prefix_dir"
# echo "use clean slate: $CLEAN_SLATE"
# echo "add shortcuts: $ADD_SHORTCUTS"
# argnum=0
# for argtxt in "$@"; do
#     echo "Arg #$argnum: $argtxt"
#     argnum=$((argnum + 1))
# done
# exit 0;

if [ -f $1 ]; then
    EXE=$1
else
    exit 1
fi


echo "Installing: $EXE"
echo "In: $prefix_dir"

if [ "$ADD_SHORTCUTS" == "false" ]; then
    # disable wine file associations, in case active (wine regedit will create them otherwise)
    export WINEDLLOVERRIDES="winemenubuilder.exe=d"
fi

# it'll be a problem if prefix doesn't exist.
# this is because the following wine-make call will create prefix-dir as an empty common-dir 
# if it doesn't exist.
if [ ! -f "$prefix_dir/dosdevices/c:/windows/explorer.exe" ]; then
    wine-make $prefix_dir --common-dir $common_dir
fi

if [ "$CLEAN_SLATE" == "true" ]; then
    # backup install prefix system.reg as well:
    cp $prefix_dir/system.reg $prefix_dir/system.reg.old
    
    # make temporary wine prefix for a clean slate diff of the registry:
    # link installation prefix as common directory, so that install files end up in the right place.
    # since linking a common-dir only makes simlinks, it can be a full wine prefix.
    wine-make $TEMPDIR --common-dir "$prefix_dir/dosdevices/c:"
else
    TEMPDIR="$prefix_dir"
fi


# make system.reg backup:
cp $TEMPDIR/system.reg $TEMPDIR/system.reg.old

# install stuff
export WINEPREFIX="$TEMPDIR"
wine $@         # use all arguments incoming, since this is just a wrapper

# wait for wine background processes to exit, since they don't write the reg files until exit
echo "Waiting for Wine services to close..."
wineserver -w

# diff registry
diff $TEMPDIR/system.reg.old $TEMPDIR/system.reg > /tmp/wine-system.reg.diff
# diff --changed-group-format='%>' --unchanged-group-format='' $TEMPDIR/system.reg.old $TEMPDIR/system.reg > $common_dir/.regs/$EXE.reg # really close to being a graceful way to do what I want, without having to rely on patch failing below.

# extract positive diff contents from diff file
rm -f $common_dir/.regs/$EXE.reg # make sure doesn't already exist
patch $common_dir/.regs/$EXE.reg < /tmp/wine-system.reg.diff -r /dev/null --no-backup-if-mismatch
echo "Made reg file: $common_dir/.regs/$EXE.reg"


if [ "$CLEAN_SLATE" == "true" ]; then
    # append registry items to installation prefix
    echo -e "\n" >> $prefix_dir/system.reg
    cat "$common_dir/.regs/$EXE.reg"  >> $prefix_dir/system.reg
    
    rm -rf "$TEMPDIR"
fi

# note: old common prefixes won't be updated with install registries
#       only new ones will inherit the registry entries
