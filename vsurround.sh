#!/bin/sh

# Creates a virutal stream using the virtual-surround-sink module
# apparently the files in hrir_listen were measured with real heads.
# see /hrir_listen/mynotes.txt for my ratings on the samples.


hrirID=1016
vsurround_path=${HOME}/.local/share/vsurround
hrirpath=${vsurround_path}/hrirs

if [ $1 = "on" ]; then
    if [ ! -f "${hrirpath}/hrir-${hrirID}.wav" ]; then
        echo "HRIR file not found"
        exit 1
    fi
    pw-cli create-node adapter '{ factory.name=support.null-audio-sink node.name=hrir-headphones media.class=Audio/Sink object.linger=1 audio.position=[FL,BL,SL,FC,FR,BR,SR] }'
    pw-jack jconvolver -s pipewire-0 ${vsurround_path}/gsx-jconvolver.config
elif [ $1 = "off" ]; then
    pw-cli destroy `pw-cli dump short Node|grep hrir-headphones|awk -F: '{print $1}'`
else
    echo "Invalid argument: $1"
    exit 1
fi
