#!/bin/bash

passwd=$(xclip -out -selection clipboard)

OLDIFS=$IFS
IFS='

'
lnum=0
for line in $passwd; do
    let lnum=lnum+2
    # echo $line
    line=${line//: /:}
    # echo $line
    IFS=':' read -r -a parts <<< "$line"
    # printf "parts: %s\n" "${parts[@]}"
    # printf "len: %s\n" "${#parts[@]}"
    if [ "${#parts[@]}" = 2 ]; then
        # printf "2\n"
        printf "%s" "${parts[1]}" | pass insert -m "${parts[0]}"
    elif [ "${#parts[@]}" = 3 ]; then
        # printf "3\n"
        printf "%s\nuser = %s" "${parts[2]}" "${parts[1]}" | pass insert -m "${parts[0]}"
    else
        echo "bad entry: line #$lnum"
    fi
done
