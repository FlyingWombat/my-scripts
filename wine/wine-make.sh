#!/usr/bin/env bash

# argument defaults and order:
prefix_path="$HOME/.wine" #1
common_dir="$HOME/wine-common" # --common-dir
COMMON=false
USE_DXVK=false
NEW_COMMON=false # internal flag

## define args
# parse arguments:
PARAMS=""
while (( "$#" )); do
  case "$1" in
    -c|--common)
#       echo c
      COMMON=true
      shift
      ;;
    -d|--dxvk)
#       echo d
      USE_DXVK=true
      shift
      ;;
    --common-dir)
#       echo cd
      COMMON=true
      common_dir="$2"
      shift 2
      ;;
    --) # end argument parsing
#       echo end
      shift
      break
      ;;
    -*|--*=) # unsupported flags
#       echo no
      echo "Error: Unsupported flag $1" >&2
      exit 1
      ;;
    *) # preserve positional arguments
#       echo pos
      PARAMS="$PARAMS $1"
      shift
      ;;
  esac
done
# set positional arguments in their proper place
eval set -- "$PARAMS"

## validate args:
# prefix_path; must be in $HOME
if [ "$1" != "" ] && [[ "$1" = "/home/"* || "$1" = "/tmp/"* ]]; then
    # make sure prefix is in $HOME
    prefix_path=$1
fi

if [ "$COMMON" == "true" ]; then
    if [ ! -d $common_dir ]; then 
        if [[ "$common_dir" = "/home/"* || "$common_dir" = "/tmp/"* ]]; then
            echo "common directory does not exist: $common_dir"
            echo "making new common directory."
            wine-common-clear "$common_dir" "$prefix_path"
            NEW_COMMON=true
        else
            echo "invalid common dir path: $common_dir"
            exit 1
        fi
    elif [ ! -z "$( ls -A $common_dir )" ]; then # check dir not empty
        # make sure directory has wine contents
        # NOTE: not a good check, since they could be empty
        if  [ ! -d "$common_dir/Program Files" ] || \
            [ ! -d "$common_dir/Program Files (x86)" ] || \
            [ ! -d "$common_dir/ProgramData" ] || \
            [ ! -d "$common_dir/users" ]; then
                echo "Error: common directory wine folders not found: $common_dir"
                exit 1
        fi
    fi
fi

# echo $1
# echo $PARAM
# echo $prefix_path
# echo $common_dir
# echo $COMMON
# echo $USE_DXVK
# echo $NEW_COMMON
# exit 0

## Script:


export WINEPREFIX=$prefix_path

# Completely disable wine system menu interaction (app menu, file ass.), globally.
# causes message:  wine: cannot find L"C:\\windows\\system32\\winemenubuilder.exe" 
export WINEDLLOVERRIDES="winemenubuilder.exe=d"

# create new prefix
# for new common, a prefix was already created.
if [ "$NEW_COMMON" == "false" ]; then
    if [ ! -d "$prefix_path" ]; then
        # wine will refuse to make a prefix in /tmp unless user makes a dir there first.
        mkdir "$prefix_path"
    fi
    wineboot -u || exit 1
fi

# wait for wine background processes to exit, since they don't write the reg files until exit
echo "Waiting for Wine services to close..."
wineserver -w

# stop wine-file associations (old way):
sed -i -e 's/winemenubuilder.exe -a -r/winemenubuilder.exe -r/g' $prefix_path/system.reg

### 2018-05-09 wine-staging-3.7 stopped working: ## just doesn't work for some reason 
# stop wine file associations (new way):
# (could instead do global disable with WINEDLLOVERRIDES="winemenubuilder.exe=d"
#echo 'Windows Registry Editor Version 5.00
#[HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\RunServices]
#"winemenubuilder"="C:\\windows\\system32\\winemenubuilder.exe -r"' > $prefix_path/#associations.reg
#cat "$prefix_path/associations.reg"
#wine regedit "$prefix_path/associations.reg"


## symlink to common folder
if [ "$COMMON" == true ]; then
    echo "linking to $common_dir"

    rm -rf "$prefix_path/dosdevices/c:/ProgramData"
    rm -rf "$prefix_path/dosdevices/c:/Program Files"
    rm -rf "$prefix_path/dosdevices/c:/Program Files (x86)"
    rm -rf "$prefix_path/dosdevices/c:/users"

    ln -sfn "$common_dir/ProgramData"         "$prefix_path/dosdevices/c:/ProgramData"
    ln -sfn "$common_dir/Program Files"       "$prefix_path/dosdevices/c:/Program Files"
    ln -sfn "$common_dir/Program Files (x86)" "$prefix_path/dosdevices/c:/Program Files (x86)"
    ln -sfn "$common_dir/users"               "$prefix_path/dosdevices/c:/users"

    # link application registries, to keep programs "installed":
    for freg in $common_dir/.regs/*.reg; do
        if [ -f $freg ]; then
            echo "adding registry: $freg"
            
            # wine regedit $freg # WTF why doesn't this work?
            # in case regedit doesn't work:
            # note: appending to the bottom isn't the right spot, 
            # but wine will re-organize the registry file on its own.
            echo -e '\n' >> $prefix_path/system.reg
            cat $freg >> $prefix_path/system.reg
        fi
    done
fi

## enable DXVK:
if [ "$USE_DXVK" == "true" ]; then
    # already exported WINEPREFIX
    echo "Setting up DXVK 32-bit..."
    setup_dxvk32
    echo "Setting up DXVK 64-bit..."
    setup_dxvk64
fi
