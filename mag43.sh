#!/usr/bin/env sh
# mag43 - sets desired 4:3 resolution to fit fullscreen
#         Then runs command
# NOTE: this is just a hack; not a comprehensive solution.

# default values:
program_resolution='800x600'
use_wine_window='false'

## define args
# parse arguments:
PARAMS=""
while (( "$#" )); do
  case "$1" in
    -w|--wine-window)
    use_wine_window='true'
    shift 1
    ;;
    -r|--resolution)
      program_resolution="$2"
      shift 2
      ;;
    --) # end argument parsing
#       echo end
      shift
      break
      ;;
    *) # preserve positional arguments
#       echo pos
      PARAMS="$PARAMS $1"
      shift
      ;;
  esac
done
# set positional arguments in their proper place
eval set -- "$PARAMS"

## validate args:

echo "Setting display to 4:3 at $program_resolution"
#xrandr --output HDMI-0 --mode 800x600 # makes plasma-desktop fit in, but won't reset.
nvidia-settings --assign CurrentMetaMode="HDMI-0: 1920x1080 { ViewPortIn=$program_resolution, ViewPortOut=1440x1080+240+0 }"


if [ "$1" == "wine" ] && [ "$use_wine_window" == "true" ]; then
    shift
    eval "wine explorer /desktop=Wine-Desktop,$program_resolution $@"
else
    eval "$@"
fi



echo "resetting display"
nvidia-settings --assign CurrentMetaMode="HDMI-0: nvidia-auto-select"
#xrandr -s 0
