#!/usr/bin/env bash

common_dir="$HOME/wine-common"
TMPDIR=/tmp/wine-common-clear-prefix

# Arg 1: new common directory
if [[ "$1" = "/home/"* || "$1" = "/tmp/"* ]]; then
    common_dir="$1"
else
    echo "invalid common dir path: $1"
    exit 1
fi

echo "clearing common directory: $common_dir"

# Arg 2: set temp directory
# for if you want to to be left over somewhere
# such as making a prefix attached to a new common.
if [[ "$2" = "/home/"* || "$2" = "/tmp/"* ]] && [ "$2" != "$common_dir" ]; then
    TMPDIR=$2
fi

# echo $common_dir
# echo $TMPDIR
# exit 0

# disable wine file associations, in case active (wine regedit will create them otherwise)
export WINEDLLOVERRIDES="winemenubuilder.exe=d"

rm -rf $common_dir
mkdir $common_dir
# mkdir $common_dir/.regs # not needed, since wine-common-install wiil create it

mkdir $TMPDIR

WINEPREFIX=$TMPDIR wineboot -u || exit 1

mv -f "$TMPDIR/dosdevices/c:/ProgramData" "$common_dir/ProgramData"
mv -f "$TMPDIR/dosdevices/c:/Program Files" "$common_dir/Program Files"
mv -f "$TMPDIR/dosdevices/c:/Program Files (x86)" "$common_dir/Program Files (x86)"
mv -f "$TMPDIR/dosdevices/c:/users" "$common_dir/users"

if [[ "$TMPDIR" = "/tmp/"* ]]; then
    rm -rf $TMPDIR
fi
