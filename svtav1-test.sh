#!/bin/sh

vid="$1"
if [ -z "$vid" ]; then echo "No video"; exit 1; fi
shift
suffix="$1"
if [ -n "$suffix" ]; then suffix="_$suffix"; shift; fi
svtparams=
pix_fmt="yuv420p10le"
method="ffmpeg"
encoder="svt-av1"
threads=
threads_ffmpeg=
threads_av1an_w=
threads_ssimulacra2="-f 12"
threads_vmaf="n_threads=8"
while [ $# -gt 0 ]; do
    case "$1" in
        -crf)
            crf_range="$2"
            shift 2;
            ;;
        -preset)
            presets="$2"
            shift 2;
            ;;
        -pix)
            pix_fmt="$2"
            shift 2;
            ;;
        -method)
            method="$2"
            shift 2;
            ;;
        -encoder)
            encoder="$2"
            shift 2;
            ;;
        -threads)
            threads="$2"
            shift 2;
            ;;
        *=*)
            svtparams="-svtav1-params $1"
            shift
            ;;
    esac
done
if [ -z "$presets" ]; then presets="2,6"; fi
if [ -z "$crf_range" ]; then crf_range="35,31,27,23,19,15,11"; fi

if [ -n "$threads" ]; then
    threads_ffmpeg="-threads $threads"
    if [ -z "$svtparams" ]; then
        svtparams="-svtav1-params lp=$threads"
    else
        svtparams="${svtparams}:lp=$threads"
    fi
    # threads_av1an_w="-w $((20/$threads))"
    threads_av1an_w="-w 1"
    threads_ssimulacra2="-f $threads"
    threads_vmaf="n_threads=$threads"
fi

# echo "#########################################" >> svtav1_test_results${suffix}.txt
echo $suffix
echo $svtparams
echo $presets

command -v ffmpeg || exit 1
fps=$(ffmpeg -i "$vid" 2>&1 | grep -oE "[0-9.]+ fps" | sed "s/ fps//")

which ffmpeg >> svtav1_test_results${suffix}.txt
which SvtAv1EncApp >> svtav1_test_results${suffix}.txt

_run(){(set -e
    local crf="$1"
    local preset="$2"
    local last="$3"
    # return
    TMP_FILE=$(mktemp -q /tmp/XXXX.mkv) || return
    echo ffmpeg -y -i "$vid" -c:v libsvtav1 -crf $crf -preset $preset $svtparams "$last" | tee -a "${TMP_FILE}.log"
    if [ ! -f "$last" ]; then
        if [ "$method" = "av1an" ]; then
            svtparams="${svtparams//-svtav1-params/--svtav1-params}"
            if [ "$encoder" = "rav1e" ]; then
                encode_args="--quantizer $(echo $crf | awk '{ print $1 * 4 }') --speed $preset"
            else
                encode_args="--crf $crf --preset $preset $svtparams"
            fi
            echo "encode args $encode_args"
            { command time --format="time: %e\ntime_user: %U\ntime_cpu_util: %P\n" av1an -y $threads_av1an_w -i "$vid" --pix-format "$pix_fmt" --sc-pix-format "$pix_fmt" -c mkvmerge -m lsmash -e $encoder -v "$encode_args" -o "$last" --log-file "${TMP_FILE}_av1an" --temp "${TMP_FILE}_av1an/" ; } 2>&1 | tee "${TMP_FILE}_av1an.txt"
            grep -E "INFO |Params:" "${TMP_FILE}_av1an.log" | tee -a "${TMP_FILE}.log"
            grep -E "time[_a-z]*:" "${TMP_FILE}_av1an.txt" | tee -a "${TMP_FILE}.log"
            grep -E "time[_a-z]*:" "${TMP_FILE}_av1an.txt" > "${last}_ffmpeg.txt"
            rm "${TMP_FILE}_av1an.log"
            rm "${TMP_FILE}_av1an.txt"
            rm "${TMP_FILE}"
        else
            if [ "$encoder" = "rav1e" ]; then
                svtparams=
                encode_args="-qp $(echo $crf | awk '{ print $1 * 4 }') -speed $preset"
            else
                encode_args="-crf $crf -preset $preset $svtparams"
            fi
            encoder="lib${encoder//-}"
            { command time --format="time: %e\ntime_user: %U\ntime_cpu_util: %P\n" ffmpeg $threads_ffmpeg -y -bitexact -i "$vid" -pix_fmt "$pix_fmt" -c:v $encoder $encode_args "$TMP_FILE" ; } 2>&1 | tee "${TMP_FILE}_ffmpeg.txt"
            mv $TMP_FILE "$last" || return
            sed $'/\r/d' "${TMP_FILE}_ffmpeg.txt" | grep -E "SVT \[|Duration: |frame=|time[_a-z]*:|Err" | tee -a "${TMP_FILE}.log"
            cat "${TMP_FILE}_ffmpeg.txt" | kak -f "%s\r<ret>xd%<a-s><a-K>frame=|Duration:|time\w*:<ret>xd" > "${last}_ffmpeg.txt"
            rm "${TMP_FILE}_ffmpeg.txt"
        fi
        stat --printf="size: %s %n\n" "$last" >> "${TMP_FILE}.log"
    fi
    if [ ! -f "${last}_ssimulacra2.txt" ]; then
        ssimulacra2_rs video $threads_ssimulacra2 -v "$vid" "$last" > "${TMP_FILE}_ssimulacra2.txt"
        mv "${TMP_FILE}_ssimulacra2.txt" "${last}_ssimulacra2.txt"
        grep -F "Video Score" -A 5 "${last}_ssimulacra2.txt" >> "${TMP_FILE}.log"
    fi
    # Note ffmpeg uses distoreted,reference order
    if [ ! -f "${last}_ssim.txt" ]; then
        ffmpeg $threads_ffmpeg -r $fps -i "$last" -r $fps -i "$vid" -lavfi "[0:v:0][1:v:0]ssim=stats_file=${TMP_FILE}_ssim.txt" -f null - 2>&1 | sed $'/\r/d' | grep -F -i "ssim" >> "${TMP_FILE}.log"
        mv "${TMP_FILE}_ssim.txt" "${last}_ssim.txt"
    fi
    if [ ! -f "${last}_psnr.txt" ]; then
        ffmpeg $threads_ffmpeg -r $fps -i "$last" -r $fps -i "$vid" -lavfi "[0:v:0][1:v:0]psnr=stats_file=${TMP_FILE}_psnr.txt" -f null - 2>&1 | sed $'/\r/d' | grep -F -i "psnr" >> "${TMP_FILE}.log"
        mv "${TMP_FILE}_psnr.txt" "${last}_psnr.txt"
    fi
    if [ ! -f "${last}_xpsnr.txt" ]; then
        ffmpeg $threads_ffmpeg -r $fps -i "$last" -r $fps -i "$vid" -lavfi "[0:v:0][1:v:0]xpsnr=stats_file=${TMP_FILE}_xpsnr.txt" -f null - 2>&1 | sed $'/\r/d' | grep -F -i "xpsnr" >> "${TMP_FILE}.log"
        mv "${TMP_FILE}_xpsnr.txt" "${last}_xpsnr.txt"
    fi
    if [ ! -f "${last}_vmaf.xml" ]; then
        ffmpeg $threads_ffmpeg -r $fps -i "$last" -r $fps -i "$vid" -lavfi "[0:v:0]setpts=PTS-STARTPTS[dist];[1:v:0]setpts=PTS-STARTPTS[ref];[dist][ref]libvmaf=feature=name=psnr|name=float_ssim:$threads_vmaf:model=path=${HOME}/vmaf_v0.6.1neg.json:log_path=${TMP_FILE}_vmaf.xml" -f null - 2>&1 | sed $'/\r/d' | grep -F -i "vmaf" >> "${TMP_FILE}.log"
        mv "${TMP_FILE}_vmaf.xml" "${last}_vmaf.xml"
        grep -F '<metric name="vmaf"' "${last}_vmaf.xml" >> "${TMP_FILE}.log"
    fi
    cat "${TMP_FILE}.log" >> svtav1_test_results${suffix}.txt
    rm "${TMP_FILE}.log"
)}

for p in ${presets//,/ }; do
    for crf in ${crf_range//,/ }; do
        _run $crf ${p} "${vid%.*}_${encoder//-}_c${crf}_s${p}${suffix}.mkv"
    done
done
