#!/bin/sh
# wrapper to apply my default settings to avifenc

crf=31
denoise=5
enable_denoise=1
yuv=
njobs=1
speed=6
mem_threshold=90  # wait until used memory is less than this percent

for arg in $@; do
    case $arg in
        "-f")
            overwrite=true
            shift
            ;;
        "-crf")
            crf="$2"
            shift 2
            ;;
        "-dn")
            denoise="$2"
            shift 2
            ;;
        "-dn0")
            enable_denoise=0
            shift
            ;;
        "-nodn")
            enable_denoise=
            shift
            ;;
        "-s")
            speed="$2"
            shift 2
            ;;
        "--yuv")
            yuv="$2"
            shift 2
            ;;
        "-j")
            njobs="$2"
            shift 2
            ;;
        "--mem-limit")
            mem_threshold="$2"
            shift 2
            ;;
    esac
done
# if [ "$1" = "-f" ]; then
#     overwrite=true
#     shift
# fi
# crf=31
# if [ "$1" = "-crf" ]; then
#     crf="$2"
#     shift 2
# fi


in="$1"
if [ -z "$2" ]; then
    out="${1%.*}.avif"
else
    out="$2"
fi
# echo "$crf $yuv $in $out"; exit

if [ -z "$overwrite" ] && [ -f "$out" ]; then
    echo "not overwriting output $out"
    exit
fi

while [ $(free | awk 'FNR == 2 {print (1-$7/$2)*100}' | cut -d "." -f1 ) -gt $mem_threshold ]; do
    echo waiting on free memory
    # random wait between 2 and 5 seconds
    sleep 5
done

ARGS="  -j $njobs
        -s $speed
        -d 10
        --min 0
        --max 63
        -a cq-level=$crf
        -a end-usage=q
        -a color:aq-mode=1
        -a color:sharpness=2
        -a color:enable-chroma-deltaq=1
        -a color:qm-min=0
        -a color:deltaq-mode=3
        "

if [ -n "$yuv" ]; then
    ARGS="$ARGS --yuv $yuv"
fi
if [ -n "$enable_denoise" ]; then
    ARGS="$ARGS -a color:enable-dnl-denoising=$enable_denoise -a color:denoise-noise-level=$denoise"
fi

echo exec ionice -c3 nice -n19 avifenc $ARGS "$in" "$out"
exec ionice -c3 nice -n19 avifenc $ARGS "$in" "$out"
