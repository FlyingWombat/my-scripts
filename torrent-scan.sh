#!/bin/sh

# orig: clamscan -ri '%F' > '/tmp/qbittorrent-scan_%N.log' && notify-send "%N" "$(grep -i -e 'infected' '/tmp/qbittorrent-scan_%N.log')"

title="$1"
savepath="$2"

logfile="/tmp/qbittorrent-scan_$title.log"
clamscan --max-filesize=4000M --max-scansize=4000M -ri "$savepath" > "$logfile"

if [ -f "$logfile" ]; then
    notify-send "$title" "$(grep -i -e 'infected' "$logfile")"
    numinfect="$(grep -i -e 'infected' "$logfile" | sed -E 's/.*([0-9]+)/\1/')"
else
    notify-send "$title" "Not scanned"
    exit 0
fi

echo "$title"
echo "# infected: $numinfect"
if [ "$numinfect" != "0" ]; then
    echo "$title" >> ~/infected-torrents.txt
fi
