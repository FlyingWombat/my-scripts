#!/usr/bin/env bash

# remove all wine extensions:
rm -f ~/.local/share/applications/wine-extension*.desktop
rm -f ~/.local/share/icons/hicolor/*/*/application-x-wine-extension*

#Next, remove the old cache:
rm -f ~/.local/share/applications/mimeinfo.cache
rm -f ~/.local/share/mime/packages/x-wine*
rm -f ~/.local/share/mime/application/x-wine-extension*

#And, update the cache:
update-desktop-database ~/.local/share/applications
update-mime-database ~/.local/share/mime/ 
