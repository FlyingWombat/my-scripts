#!/usr/bin/env bash


install_method () {
    echo "setting up DXVK in: $1..."
    WINESYSDIR=$( WINEPREFIX="$1" winepath -u c:\\windows\\system32 2> /dev/null )
    if [[ ${WINESYSDIR} == *"/system32" ]]; then # is 32-bit prefix
        echo "Setting up DXVK 32-bit..."
        WINEPREFIX="$1" setup_dxvk32
    else
        echo "Setting up DXVK 32-bit..."
        WINEPREFIX="$1" setup_dxvk32
        echo "Setting up DXVK 64-bit..."
        WINEPREFIX="$1" setup_dxvk64
    fi
}


for windir in $HOME/.wine*dxvk*/; do
    install_method "$windir"
done


if [ -f 'dxvk-prefix-list.txt' ]; then
    while read prefixpath ; do
        if [ -d "$prefixpath" ]; then
            install_method "$prefixpath"
        else
            echo "Wine prefix not found: $prefixpath"
        fi
    done < dxvk-prefix-list.txt
fi
