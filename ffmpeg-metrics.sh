#!/bin/sh
set -e
case "$1" in
    "vmaf")
        mode="vmaf"
        shift
        ;;
    "xpsnr")
        mode="xpsnr"
        shift
        ;;
esac
ref="$1"
dist="$2"

fps=$(ffmpeg -i "$ref" 2>&1 | grep -oE "[0-9.]+ fps" | sed "s/ fps//")
TMP_FILE=$(mktemp -q /tmp/XXXX.log) || return

case "$mode" in
    "xpsnr")
        ffmpeg -r $fps -i "$dist" -r $fps -i "$ref" -lavfi "[0:v:0][1:v:0]xpsnr=stats_file=${TMP_FILE}" -f null -
        mv "${TMP_FILE}" "${dist}_xpsnr.txt"
        ;;
    "vmaf")
        ffmpeg -r $fps -i "$dist" -r $fps -i "$ref" -lavfi "[0:v:0]setpts=PTS-STARTPTS[dist];[1:v:0]setpts=PTS-STARTPTS[ref];[dist][ref]libvmaf=feature=name=psnr|name=float_ssim:n_threads=8:model=path=${HOME}/vmaf_v0.6.1neg.json:log_path=${TMP_FILE}" -f null -
        mv "${TMP_FILE}" "${dist}_vmaf.xml"
        ;;
    *)
        exit 1
        rm "$TMP_FILE"
        ;;
esac
