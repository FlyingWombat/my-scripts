#!/bin/sh

log=/tmp/ranger-open.log
path=${1#file://}
echo "$path"
if [ -d $path ]
then
     ( $(command -v $TERMCMD) -e $(command -v ranger) "$path" > $log 2>&1 & )
else
     ( $(command -v $TERMCMD) -e $(command -v ranger) --selectfile="$path" > $log 2>&1 & )
fi
