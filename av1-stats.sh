#!/bin/sh

rm av1_test_stats.tsv
# make tsv of data
IFS=$'\n'
for outfile in $(fd -tf '_\w*av1.*\.mkv$'); do
    echo "file: $outfile" >> av1_test_stats.tsv
    stat --printf="size: %s\n" "$outfile" >> av1_test_stats.tsv
    ffmpeg -i "$outfile" 2>&1 | grep -E "Duration:" >> av1_test_stats.tsv
    grep -F "Mean: " -A 4 "${outfile}_ssimulacra2.txt" | sed -e 's/^/ssimulacra2 /' >> av1_test_stats.tsv
    # cat "${outfile}_ssimulacra2.txt" | kak -f '/Frame 0:<ret><a-i>p|sort -k2V | tail -n1<ret>ghf:Gldb|awk '\''{ print $1+1 }'\''x*%<a-s><a-K><ret>d' >> av1_test_stats.tsv
    grep -F "XPSNR average," "${outfile}_xpsnr.txt" >> av1_test_stats.tsv
    grep -F '<metric name="vmaf"' "${outfile}_vmaf.xml" >> av1_test_stats.tsv
    grep -E "time[_a-z]*:" "${outfile}_ffmpeg.txt" >> av1_test_stats.tsv
done

kak -f '%sDuration: <ret>f,;Gld<lt><a-B>_|awk -F: '\''{ print $1*3600+$2*60+$3 ;}'\''<ret>' av1_test_stats.tsv
# kak -f '%sframe=<ret>xs\w+=\s*\d<ret><a-E>L<a-K>fps|bitrate|speed<ret>dxsspeed=<ret><a-e>;dxs=<ret>c: <esc><a-E>lc<ret><esc>xsbitrate:<ret>Fk;Gldghea_Kbps<esc>' av1_test_stats.tsv
kak -f '%sXPSNR<ret>F,Ld<a-e><a-e>dbPa: <esc>f(mdxs[YUV]:<ret>iXPSNR_<esc>xs:<ret><a-e>lc<ret><esc>' av1_test_stats.tsv
# old format; default time output
# kak -f '%s%CPU<ret>lGldxs\d[a-z%]<ret>;hli <esc>Ed<a-b>Pa: <esc>h<esc><a-b>itime_<esc>xselapsed<ret>elll<a-e>|awk -F: '\''{ print $1*60+$2 ;}'\''<ret>xs:<ret><a-e>lc<ret><esc>' av1_test_stats.tsv
kak -f '%smetric name=<ret><a-E>L<a-;>GhdglHHdxs=<ret>c: <esc><a-b>hlivmaf_<esc>xs"<ret>dxs:<ret><a-e>a<esc>lc<ret><esc>' av1_test_stats.tsv

# convert "name: value" lines to tsv; grouped by sections starting with "file:"
# kak -f '<percent><a-s><lt><percent>s^$<ret>d,/file:<ret>?<ret>Kx<a-s>ghF:Hygg<a-O>k<a-P>a<tab><esc><percent>sfile:<ret><a-O><a-i>p<a-s>A<tab><esc>xs:<ret>Ghddx<a-j>' av1_test_stats.tsv

# parse "name: value" lines into tsv; more robust
python - << ENDPYTHON
import csv
with open("av1_test_stats.tsv", "r+") as f:
    rows = []
    cols = {}
    for line in f:
        line = line.strip()
        if not line:
            continue
        k, v = line.split(": ", 1)
        if k == "file":
            if cols.values():
                rows.append(cols)
            cols = {}
        cols[k] = v
    if cols.values():
        rows.append(cols)
    header = list(dict.fromkeys(k for row in rows for k in row.keys()).keys())
    f.truncate(0)
    f.seek(0)
    writer = csv.DictWriter(f, fieldnames=header, delimiter='\t')
    writer.writeheader()
    for row in rows:
        writer.writerow(row)
ENDPYTHON
