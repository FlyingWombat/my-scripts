#!/usr/bin/env python3

import csv
import glob
import json
import os
import shlex
import subprocess

# 3x as fast as subproc versions
import re
def _grep(rxp, lines, *, A=0, only=False) -> list[str]:
    if isinstance(lines, str):
        lines = lines.split('\n')
    outlines = []
    _A = 0
    for line in lines:
        if _A > 0:
            outlines.append(line)
            _A -= 1
        elif _match := re.search(rxp, line):
            if only:
                outlines.append(_match.group())
            else:
                outlines.append(line)
            _A = A
    return outlines

RE_ffmpeg = re.compile(r"Duration: [0-9:.]+|, [0-9]+x[0-9]+\b")
RE_ssimulacra2 = re.compile("Mean: ")
RE_ssimulacra2_frame = re.compile("Frame ")
RE_xpsnr = re.compile("XPSNR average,")
RE_xpsnr_mean_toks = re.compile('[A-Za-z]: [0-9.]+')
RE_vmaf = re.compile('<metric name="vmaf"')
RE_vmaf_psnr = re.compile('<metric name="psnr_y"')
RE_vmaf_ssim = re.compile('<metric name="float_ssim"')
RE_time = re.compile("time[_a-z]*:")
RE_source = re.compile(r"_sample_\w*av1\w*_")

with open("av1_test_stats.tsv", "w") as f:
    rows = []
    # make tsv of data
    for outfile in sorted(glob.glob("*_*av1*.mkv")):
        cols = {}
        cols["file"] = outfile
        settings_tokens = outfile.split('_sample_')[1].removesuffix('.mkv').split('_')
        cols["encoder"] = settings_tokens[0]
        cols["crf"] = settings_tokens[1]
        cols["setting"] = '_'.join(settings_tokens[2:])
        size = os.stat(outfile).st_size
        cols["size"] = size
        src_file = re.split(RE_source, outfile)[0] + "_sample.mkv"
        try:
            src_size = os.stat(src_file).st_size
            cols["src_size"] = src_size
        except FileNotFoundError:
            pass
        ffmpeg_txt = subprocess.run(["ffmpeg", "-i", outfile], capture_output=True, encoding="utf-8").stderr
        ffmpeg_txt = _grep(RE_ffmpeg, ffmpeg_txt, only=True)
        res = ffmpeg_txt[1].removeprefix(", ").split("x")
        cols["pixels_w"] = res[0]
        cols["pixels_h"] = res[1]
        hms = [float(x) for x in ffmpeg_txt[0].removeprefix("Duration: ").split(':')]
        duration = hms[0]*3600 + hms[1]*60 + hms[2]
        cols["Duration"] = duration
        if os.path.isfile(f"{outfile}_ssimulacra2.txt"):
            with open(f"{outfile}_ssimulacra2.txt") as txt:
                ssimulacra2_txt = txt.readlines()
                ssimulacra2_min = _grep(RE_ssimulacra2_frame, ssimulacra2_txt)
                ssimulacra2_min = list(set(ssimulacra2_min))  # unique values, because I've changed some to have a summary at the top
                ssimulacra2_min = sorted(ssimulacra2_min, key = lambda x: float(x.split(': ')[1]))[0]
                ssimulacra2_min = ssimulacra2_min.split(': ')[1].strip()
                cols["ssimulacra2 Min"] = ssimulacra2_min
                ssimulacra2_txt = _grep(RE_ssimulacra2, ssimulacra2_txt, A=4)
                ssimulacra2_txt = [x.strip().split(": ") for x in ssimulacra2_txt]
                cols.update({f"ssimulacra2 {k}": v for k, v in ssimulacra2_txt})
        if os.path.isfile(f"{outfile}_xpsnr.txt"):
            with open(f"{outfile}_xpsnr.txt") as txt:
                xpsnr_txt = _grep(RE_xpsnr, txt.readlines())[0].strip()
                xpsnr_txt = xpsnr_txt.split(", ")[1].split('(')[0]
                frames, xpsnr_txt = xpsnr_txt.split(" frames")
                cols["frames"] = frames
                xpsnr_txt = xpsnr_txt.upper()  # new upstreamed version prints lower case
                xpsnr_txt = [f"XPSNR_{x}".split(": ") for x in re.findall(RE_xpsnr_mean_toks, xpsnr_txt)]
                cols.update({k: v for k, v in xpsnr_txt})
        if os.path.isfile(f"{outfile}_vmaf.xml"):
            with open(f"{outfile}_vmaf.xml") as txt:
                vmaf_txt = txt.readlines()
                vmaf_scores = _grep(RE_vmaf, vmaf_txt)[0].strip()
                vmaf_scores = vmaf_scores.split('name="vmaf" ')[1].split(' ')[:-1]
                vmaf_scores = [ x.split('=') for x in vmaf_scores]
                vmaf_scores = {f"vmaf_{k}": v.strip('\"') for k, v in vmaf_scores}
                cols.update(vmaf_scores)
                # get psnr and ssim if available
                vmaf_scores = _grep(RE_vmaf_psnr, vmaf_txt)
                if vmaf_scores:
                    vmaf_scores = vmaf_scores[0].strip()
                    vmaf_scores = vmaf_scores.split('name="psnr_y" ')[1].split(' ')[:-1]
                    vmaf_scores = [ x.split('=') for x in vmaf_scores]
                    vmaf_scores = {f"psnr_y_{k}": v.strip('\"') for k, v in vmaf_scores}
                    cols.update(vmaf_scores)
                vmaf_scores = _grep(RE_vmaf_ssim, vmaf_txt)
                if vmaf_scores:
                    vmaf_scores = vmaf_scores[0].strip()
                    vmaf_scores = vmaf_scores.split('name="float_ssim" ')[1].split(' ')[:-1]
                    vmaf_scores = [ x.split('=') for x in vmaf_scores]
                    vmaf_scores = {f"ssim_{k}": v.strip('\"') for k, v in vmaf_scores}
                    cols.update(vmaf_scores)
        cols["bitrate_kbps"] = f"{size*8/duration/1000:8.3f}"
        if os.path.isfile(f"{outfile}_perf.json"):
            with open(f"{outfile}_perf.json") as txt:
                perf_counters = {}
                for line in txt:  # perf outputs multiple top-level json objects
                    line = json.loads(line)
                    perf_counters[line["event"]] = line
                cols["time"] = float(perf_counters["duration_time"]["counter-value"]) / 1e9  # was ns
                cols["time_user"] = float(perf_counters["user_time"]["counter-value"]) / 1e9 # was ns
                cols["time_cpu_util"] = perf_counters["task-clock"]["metric-value"]
                cols["instructions"] = perf_counters["instructions"]["counter-value"]
                cols["cycles"] = perf_counters["cycles"]["counter-value"]
        elif os.path.isfile(f"{outfile}_ffmpeg.txt"):
            with open(f"{outfile}_ffmpeg.txt") as txt:
                runtime_txt = [x.strip().split(": ") for x in _grep(RE_time, txt.readlines())]
                cols.update({k: v for k, v in runtime_txt})
                cols["time_cpu_util"] = float(cols["time_cpu_util"].removesuffix("%")) / 100
        rows.append(cols)

    # write into tsv
    header = list(dict.fromkeys(k for row in rows for k in row.keys()).keys())
    writer = csv.DictWriter(f, fieldnames=header, delimiter='\t')
    writer.writeheader()
    for row in rows:
        writer.writerow(row)


# with open("av1_test_stats.tsv", "w") as f:
#     # make tsv of data
#     for outfile in sorted(glob.glob("*_*av1*.mkv")):
#         print(f"file: {outfile}", file=f)
#         print("size: {}".format(os.stat(outfile).st_size), file=f)
#         # print(subprocess.run(f""" ffmpeg -i '{outfile}' 2>&1 | grep -E "Duration:" """, shell=True, capture_output=True, encoding="utf-8").stdout, file=f, end='')
#         ffmpeg_result = subprocess.run(["ffmpeg", "-i", outfile], stderr=subprocess.PIPE)
#         grep_result = subprocess.run(["grep", "-E", "Duration:"], input=ffmpeg_result.stderr, stdout=subprocess.PIPE)
#         print(grep_result.stdout.decode('utf-8'), file=f, end='')
#         # ssimulacra2_txt = subprocess.run(["grep", "-F", "Mean: ", "-A", "4", f"{outfile}_ssimulacra2.txt" ], capture_output=True, encoding="utf-8").stdout
#         ssimulacra2_txt = subprocess.run(shlex.split(f"grep -F 'Mean: ' -A 4 '{outfile}_ssimulacra2.txt'"), capture_output=True, encoding="utf-8").stdout
#         print('\n'.join(["ssimulacra2 " + x for x in ssimulacra2_txt.split('\n')[:-1]]), file=f)
#         # cat f"{outfile}_ssimulacra2.txt" | kak -f r"""/Frame 0:<ret><a-i>p|sort -k2V | tail -n1<ret>ghf:Gldb|awk '{ print $1+1 }'x*%<a-s><a-K><ret>d""" >> av1_test_stats.tsv
#         print(subprocess.run(["grep", "-F", "XPSNR average,", f"{outfile}_xpsnr.txt"], capture_output=True, encoding="utf-8").stdout, file=f, end='')
#         print(subprocess.run(["grep", "-F", '<metric name="vmaf"', f"{outfile}_vmaf.xml"], capture_output=True, encoding="utf-8").stdout, file=f, end='')
#         print(subprocess.run(["grep", "-E", "time[_a-z]*:", f"{outfile}_ffmpeg.txt"], capture_output=True, encoding="utf-8").stdout, file=f, end='')

# from plumbum.cmd import ffmpeg, grep, kak
# from plumbum import ERROUT
# with open("av1_test_stats.tsv", "w") as f:
#     # make tsv of data
#     for outfile in sorted(glob.glob("*_*av1*.mkv")):
#         print(f"file: {outfile}", file=f)
#         print("size: {}".format(os.stat(outfile).st_size), file=f)
#         print(((ffmpeg["-i", f"{outfile}"] >= ERROUT) | grep["-E", "Duration:"])(retcode=None), file=f, end='')
#         # ffmpeg_result = (ffmpeg["-i", f"{outfile}"]).run(retcode=None)
#         # grep_result = (grep["-E", "Duration:"] << ffmpeg_result[2])()
#         # print(grep_result, file=f, end='')
#         ssimulacra2_txt = grep["-F", "Mean: ", "-A", "4", f"{outfile}_ssimulacra2.txt" ]()
#         print('\n'.join(["ssimulacra2 " + x for x in ssimulacra2_txt.split('\n')[:-1]]), file=f)
#         print(grep["-F", "XPSNR average,", f"{outfile}_xpsnr.txt"](), file=f, end='')
#         print(grep["-F", '<metric name="vmaf"', f"{outfile}_vmaf.xml"](), file=f, end='')
#         print(grep["-E", "time[_a-z]*:", f"{outfile}_ffmpeg.txt"](), file=f, end='')

# exit()

# # pure python was 40% faster
# subprocess.run(["kak", "-f", r"""%sDuration: <ret>f,;Gld<lt><a-B>_|awk -F: '{ print $1*3600+$2*60+$3 ;}'<ret>""", "av1_test_stats.tsv"])
# # kak -f r"""%sframe=<ret>xs\w+=\s*\d<ret><a-E>L<a-K>fps|bitrate|speed<ret>dxsspeed=<ret><a-e>;dxs=<ret>c: <esc><a-E>lc<ret><esc>xsbitrate:<ret>Fk;Gldghea_Kbps<esc>""" av1_test_stats.tsv
# subprocess.run(["kak", "-f", r"""%sXPSNR<ret>F,Ld<a-e><a-e>dbPa: <esc>f(mdxs[YUV]:<ret>iXPSNR_<esc>xs:<ret><a-e>lc<ret><esc>""", "av1_test_stats.tsv"])
# # old format; default time output
# # kak -f r"""%s%CPU<ret>lGldxs\d[a-z%]<ret>;hli <esc>Ed<a-b>Pa: <esc>h<esc><a-b>itime_<esc>xselapsed<ret>elll<a-e>|awk -F: '{ print $1*60+$2 ;}'<ret>xs:<ret><a-e>lc<ret><esc>""" av1_test_stats.tsv
# subprocess.run(["kak", "-f", r"""%smetric name=<ret><a-E>L<a-;>GhdglHHdxs=<ret>c: <esc><a-b>hlivmaf_<esc>xs"<ret>dxs:<ret><a-e>a<esc>lc<ret><esc>""", "av1_test_stats.tsv"])

# convert "name: value" lines to tsv; grouped by sections starting with "file:"
# kak -f '<percent><a-s><lt><percent>s^$<ret>d,/file:<ret>?<ret>Kx<a-s>ghF:Hygg<a-O>k<a-P>a<tab><esc><percent>sfile:<ret><a-O><a-i>p<a-s>A<tab><esc>xs:<ret>Ghddx<a-j>' av1_test_stats.tsv

# # parse "name: value" lines into tsv; more robust
# with open("av1_test_stats.tsv", "r+") as f:
#     rows = []
#     cols = {}
#     for line in f:
#         line = line.strip()
#         if not line:
#             continue
#         k, v = line.split(": ", 1)
#         if k == "file":
#             if cols.values():
#                 rows.append(cols)
#             cols = {}
#         cols[k] = v
#     if cols.values():
#         rows.append(cols)
#     header = list(dict.fromkeys(k for row in rows for k in row.keys()).keys())
#     f.truncate(0)
#     f.seek(0)
#     writer = csv.DictWriter(f, fieldnames=header, delimiter='\t')
#     writer.writeheader()
#     for row in rows:
#         writer.writerow(row)
